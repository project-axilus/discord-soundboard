# [Discord Soundboard][discord_invite]
_Providing a template for creating Discord soundboard bots._

## Dependencies
In order for voice to function properly, you need to install: 

- `ffmpeg`
- `libffi-dev`
- `libopus-dev`

Finally, setup a virtual Python environmennt (preferably using `poetry shell`) with the dependencies listed in `pyproject.toml`.

## Usage
For each available bot, you can run:

```bash
./bot.py BOT_NAME
```

## Folder Structure
Each bot should have its own folder with both a `sound` directory, and a `config.yaml` file.
The `sound` directory should only contain a collection of audio files (preferably MP3).
The tokens for each bot are assumed to be avaiable as either:

- environmennt variables, 
- or as variables in a `.env` file (see `.env.example`).

## License
Copyright © Mr Axilus.
This project is licensed under [CC BY-NC-SA 4.0][license].

[discord_invite]: https://discord.gg/rUpJtsv
[license]: https://creativecommons.org/licenses/by-nc-sa/4.0/
