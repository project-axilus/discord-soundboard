#!/usr/bin/env python3

import glob
import logging
import os
import random
import sys
import time

import discord as dc
import discord.ext.commands as dc_cm
import dotenv as de
import fuzzywuzzy as fw
import fuzzywuzzy.process
import yaml as ym

# Setup imported modules.
random.seed()
de.load_dotenv()
logging.basicConfig(level=logging.INFO)

# Load library required for audio communication.
if not dc.opus.is_loaded():
    try:
        dc.opus.load_opus("opus")
    except:
        logging.warning("Opus may not have loaded.")

# Retrieve the path to the bot's configuration folder.
if len(sys.argv) < 2:
    logging.error("No bot folder specified.")
    sys.exit(1)
ROOT = sys.argv[1]

# Retrieve configuration settings.
SETTING = {}
with open(f"{ROOT}/config.yaml") as file:
    SETTING = ym.safe_load(file)
if len(SETTING) == 0:
    logging.error("No config file found in bot folder provided.")
    sys.exit(1)

# Setup global objects.
BOT = dc_cm.Bot(
    command_prefix=SETTING["command_prefix"], description=SETTING["description"]
)
VOICE = None

# Locate path to sound files.
SOUND_PATH = f"{ROOT}/sound"
sounds = ["channel_change_sound", "kick_sound", "summon_sound"]
for sound in sounds:
    if sound in SETTING:
        SETTING[
            sound
        ] = f"{SOUND_PATH}/{SETTING[sound]}"  # Convert sound filename to full path.


def play_sound(path: str) -> None:
    """Play a sound by specifiying its local name."""
    logging.info(f"Playing sound at path: {path}")
    audio = dc.FFmpegPCMAudio(path)
    VOICE.play(audio)


@BOT.event
async def on_ready() -> None:
    """Notify of successful login."""
    logging.info(f"Logged in as {BOT.user.name} with ID of {BOT.user.id}")


@BOT.event
async def on_voice_state_update(
    member: dc.Member, before: dc.VoiceState, after: dc.VoiceState
) -> None:
    """Notify users whenever someone joins or leaves a voice channel."""
    # Ignore if bot is not in voice channel.
    if not VOICE:
        logging.debug("Bot not in voice channel.")
        return

    # Ignore if user is in the same channel after state change.
    if before.channel == after.channel:
        logging.debug("User changed state in same voice channel.")
        return

    # Ignore if the bot is in the middle of something.
    if VOICE.is_playing():
        logging.debug("Bot is already playing sound.")
        return

    # Ignore if user is another bot, which is determined by its role.
    roles = list(map(str, member.roles))
    if SETTING["bot_role"] in roles:
        logging.debug("State change is from another bot.")
        return

    # Ignore if channel change sound not specified.
    if "channel_change_sound" not in SETTING:
        logging.debug("Channel change sound not specified.")
        return

    # Play the channel change sound.
    play_sound(SETTING["channel_change_sound"])


@BOT.command()
async def summon(context: dc_cm.Context) -> None:
    """Join the summoner's voice channel upon request."""
    global VOICE
    logging.info("Summoning bot.")

    # Leave existing voice channel.
    channel = context.message.author.voice.channel
    if VOICE:
        logging.debug("Bot in another voice channel.")
        kick(context)

    # Join summoner's voice channel.
    VOICE = await channel.connect()

    # Play the summon sound after a random delay up to a second.
    time.sleep(random.random())
    play_sound(SETTING["summon_sound"])


@BOT.command()
async def kick(context: dc_cm.Context) -> None:
    """Disconnect from any voice channel upon request."""
    global VOICE
    logging.info("Disconnecting from voice channel.")

    # Ignore if not in a voice channel.
    if not VOICE:
        logging.debug("Bot is not in a voice channel.")
        return

    # Stop the bot from continuing previous sounds.
    if VOICE.is_playing():
        logging.info("Bot is currently playing sound; stopping.")
        VOICE.stop()

    # Play the kick sound and wait for it to finish.
    time.sleep(random.random())
    play_sound(SETTING["kick_sound"])
    while VOICE.is_playing():
        logging.info("Waiting on kick sound to end.")
        time.sleep(0.2)  # Average human response time.

    # Disconnect and reset the voice channel connection.
    await VOICE.disconnect()
    VOICE = None


@BOT.command()
async def search(context: dc_cm.Context, *, name: str = None) -> None:
    """Search for available sound clips."""
    logging.info("Searching for available sound clips.")

    # Acquire and format a list of available clips.
    clips = []
    for clip in glob.glob(SOUND_PATH + "/*.mp3"):
        clip = clip[len(SOUND_PATH) + 1 : -4]
        clip = clip.replace("_", " ")
        clip = clip.title()
        clips.append(clip)
    clips.sort()

    # Filter the list based on the user's provided search term.
    if name:
        logging.info("Filtering sounds by search term.")
        clips = fw.process.extract(name, clips)
        clips = [c[0] for c in clips if c[1] > 55]

    # Display the resulting list of available sounds.
    channel = context.message.channel
    await channel.send("\n".join(clips))


@BOT.command()
async def play(context: dc_cm.Context, *, name: str = None) -> None:
    """Request the bot to play any available sound clip."""
    # Ignore if the bot is not in any voice channels.
    if not VOICE:
        logging.debug("Bot is not in any voice channel.")
        return

    # Find the closest matching clip or provide a random one.
    clips = glob.glob(SOUND_PATH + "/*.mp3")
    clip = random.choice(clips)
    if name:
        logging.info("Picking the closest matching sound clip.")
        clip, _ = fw.process.extractOne(name, clips)

    # Stop the player if audio is currently being played.
    if VOICE.is_playing():
        logging.info("Stopping existing audio playback.")
        VOICE.stop()

    # Play the requested sound clip.
    play_sound(clip)


@BOT.command()
async def stop(context: dc_cm.Context) -> None:
    """Stop any audio that is currently playing."""
    # Ignore if nothing is playing.
    if not VOICE:
        logging.debug("Bot is not in a voice channel.")
        return

    # Stop the audio and reset the player.
    VOICE.stop()


BOT.run(os.environ.get(SETTING["token_env_name"]))
